---
title: "Naiveproxy Setup"
date: 2020-03-19T13:26:14+08:00
---

## 简介

这是我的第一个博文，也是干货满满的技术贴。因为最近github上惊现v2ray的流量识别测试，引发不小的慌乱。然而，我们的开源社区早就有人在开发替代品。在[mastodon](https://joinmastodon.org/)一位嘟友的推荐下，我了解到了一款强大的翻墙软件：[naiveproxy](https://github.com/klzgrad/naiveproxy)。作者的思路很清晰，翻墙软件混淆、自己造加密协议的时代已经过去，现有的例子已经证明：混淆本身会成为一个攻击对象，自己造的加密协议终归是敌不过专门研究加密的学者做出的东西。所以作者的思路是采用最常用的浏览器的网络栈来躲避审查，审查最常用的浏览器的网络栈的代价是很高的，于是大大降低了被查到的风险。由于naiveproxy是较新的翻墙软件，下面将介绍如何快速搭建一个naiveproxy服务器。(注：一键配置脚本已更新，见最下面一条)

## 准备

由于使用的是TLS流量，需要申请域名，来申请let's encrypt的免费TLS证书。本教程只介绍如何搭建naiveproxy服务器，对于域名的申请和注册，已经有较好的教程：1、[freenom免费域名申请](https://www.jianshu.com/p/e02371079daa)   2、[ 将您的域名服务器更改为 Cloudflare](https://support.cloudflare.com/hc/zh-cn/articles/205195708-%E5%B0%86%E6%82%A8%E7%9A%84%E5%9F%9F%E5%90%8D%E6%9C%8D%E5%8A%A1%E5%99%A8%E6%9B%B4%E6%94%B9%E4%B8%BA-Cloudflare)。值得注意的是更改域名服务器之后，要添加一个你的服务器的DNS record。这样你的域名才会被解析到你的服务器IP。

由于所用软件都是从官网下载的最新binary，此教程适用于各种linux发行版。

## 安装naiveproxy、caddy

naiveproxy需要配合转发代理服务器使用，caddy是具有自动TLS功能的HTTPS服务器，naiveproxy官方推荐。下面介绍如何安装独立于系统的naiveproxy和caddy binary。

### 安装caddy

安装具有forward proxy支持的caddy可执行文件：

```shell
curl https://getcaddy.com | bash -s personal http.forwardproxy
```
添加caddy systemd启动服务，用你喜欢的文本编辑器（此处为vim），打开/usr/lib/systemd/system/caddy.service

```shell
vim /usr/lib/systemd/system/caddy.service
```
然后添加以下内容保存并退出：

```systemd
[Unit]
Description=Caddy HTTP/2 web server
Documentation=https://caddyserver.com/docs
After=network-online.target
Wants=network-online.target systemd-networkd-wait-online.service
StartLimitIntervalSec=14400
StartLimitBurst=10

[Service]
User=root
Group=root
ExecStart=/usr/local/bin/caddy -log stdout -agree -conf /etc/caddy/caddy.conf -root=/usr/share/caddy
ExecReload=/usr/bin/kill -USR1 $MAINPID

# Do not allow the process to be restarted in a tight loop. If the
# process fails to start, something critical needs to be fixed.
Restart=on-abnormal

# Use graceful shutdown with a reasonable timeout
KillMode=mixed
KillSignal=SIGQUIT
TimeoutStopSec=5s

LimitNOFILE=1048576
LimitNPROC=512

[Install]
WantedBy=multi-user.target
```
### 安装naiveproxy

naiveproxy需要[nspr](https://developer.mozilla.org/en-US/docs/Mozilla/Projects/NSPR)、[nss](https://developer.mozilla.org/en-US/docs/Mozilla/Projects/NSS)作为运行依赖，安装naiveproxy之前先安装nspr、nss。

运行以下命令下载最新版naiveproxy：
```shell
curl -s "https://api.github.com/repos/klzgrad/naiveproxy/releases/latest" | \ 
    grep linux-x64 | grep browser_download_url | \
    cut -d : -f 2,3 | tr -d \" | wget -qi -
```

解压下载的压缩包：

```shell
tar -xvf naive*tar.xz
```

将naiveproxy安装到/usr/local/bin/目录

```shell
# cp naive*/naive /usr/local/bin/naiveproxy
```

添加naiveproxy systemd启动服务，用你喜欢的文本编辑器（此处为vim），打开/usr/lib/system/system/naiveproxy.service:

```shell
vim /usr/lib/systemd/system/naiveproxy.service
```

添加以下内容并保存：

```systemd
[Unit]
Description=naiveproxy - Make a fortune quietly
Documentation=https://github.com/klzgrad/naiveproxy/blob/master/USAGE.txt
After=network.target nss-lookup.target
Wants=network-online.target

[Service]
# If the version of systemd is 240 or above, then uncommenting Type=exec and commenting out Type=simple
Type=exec
#Type=simple
# Runs as root or add CAP_NET_BIND_SERVICE ability can bind 1 to 1024 port.
# This service runs as root. You may consider to run it as another user for security concerns.
# By uncommenting User=naiveproxy and commenting out User=root, the service will run as user naiveproxy.

User=root
#User=naiveproxy
#AmbientCapabilities=CAP_NET_BIND_SERVICE
ExecStart=/usr/local/bin/naiveproxy /etc/naiveproxy/config.json
Restart=on-failure
# Don't restart in the case of configuration error
RestartPreventExitStatus=23

[Install]
WantedBy=multi-user.target
```

## 配置naiveproxy、caddy

创建配置文件夹：

```shell
mkdir /etc/naiveproxy/ /etc/caddy
```
### 配置caddy

用文本编辑器编辑caddy的配置：

```shell
vim /etc/caddy/caddy.conf
```
添加以下配置：

```caddy
domain.example
root /var/www/html
tls myemail@example.com
forwardproxy {
  basicauth user pass
  hide_ip
  hide_via
  probe_resistance secret.localhost
  upstream http://127.0.0.1:8080
}
```

其中**domain.example**为服务器域名，**myemail@example.com**为你的email用于let's encrypt申请证书，**user**和**pass**分别为用户名和密码。

### 配置naiveproxy

用文本编辑器编辑naiveproxy的配置：

```shell
vim /etc/naiveproxy/config.json
```

添加以下配置：

```json
{
    "listen": "http://127.0.0.1:8080",
    "padding": "true"
}
```

## 启动服务

使用systemd启动服务，首先重新导入service：

```shell
# systemctl daemon-reload
```

设置开机自启，并启动caddy、naiveproxy服务：

```shell
# systemctl enable naiveproxy caddy --now
```

自此服务器端配置结束。

## 配置客户端

在客户端按照同样的方式安装naiveproxy，不同的是，配置文件有所改变：

```shell
vim /etc/naiveproxy/config.json
```

```json
{
    "listen": "socks://127.0.0.1:1080",
    "proxy": "https://user:pass@domain.example",
    "padding": "true"
}
```

其中**user**、**pass**为服务器相应的用户名和密码，**domain.example**为服务器域名。

配置好之后启动服务：

```shell
# systemctl start naiveproxy
```

或者开机自启并运行：

```shell
# systemctl enable naiveproxy --now
```

最后设置好各种软件的代理就可以用了。

## 一键脚本

一键配置脚本下载地址：[https://gitlab.com/hououin/naiveproxy_scripts](https://gitlab.com/hououin/naiveproxy_scripts)，其中naiveproxy_setup脚本用于配置服务器，naive_update脚本用于更新naiveproxy和caddy。

注意要给予naiveproxy_setup和naive_update可执行权限：

```shell
chmod +x naiveproxy_update naive_update
```

运行脚本（naiveproxy_setup所在目录）

```shell
./naiveproxy_setup
```

naive_update同理。
